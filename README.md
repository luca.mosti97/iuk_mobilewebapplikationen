# ToDo Progressive Web App
This project's goal was to build a Progressive Web App (PWA)

## Team
The team consisted of **Michael Str&auml;ssle**, **Luca Mosti** and **Benjamin Gut**

## Information
In the backend Node.js was used to create an Express.js as REST-API
The frontend was hand-built with HTML, bootstrap was used for styling

## How to run locally
* Clone the repository from [GitLab](https://gitlab.com/luca.mosti97/iuk_mobilewebapplikationen)
* Install the required components with `npm install`
* Run `node index.js` in root folder and go to `localhost:4000`


## Live version
To see a live-version of the PWA go to [ToDo](https://safe-peak-12131.herokuapp.com/#)

