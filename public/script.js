var tasks;
var id;
let db;

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./sw.js');
}

window.onload = function createDatabase() {   
    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
    window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
  
    //Open Database
    const DBOpenRequest = window.indexedDB.open("toDoApp", 4);
  
    //Check database errors when creating
    DBOpenRequest.onerror = function(event) {
      console.log("Initialisierung der Datenbank fehlgeschlagen");
    };
    DBOpenRequest.onsuccess = function(event) {
        console.log("Initialisierung der Datenbank erfolgreich");
        //Store the result of opening the database in the db variable
        db = DBOpenRequest.result;
        getAll("false");
    };

    // This event handles the event whereby a new version of the database needs to be created
    // Either one has not been created before, or a new version number has been submitted via the
    // window.indexedDB.open line above
    DBOpenRequest.onupgradeneeded = function(event) {
        db = event.target.result;

        db.onerror = function(event) {
            console.log("Fehler beim Laden der Datenbank")
        };

        // Create an objectStore for this database
        let objectStore = db.createObjectStore("toDoApp", { keyPath: "id", autoIncrement:true});

        // define what data items the objectStore will contain
        objectStore.createIndex("taskTitle", "taskTitle", {unique: false });
        objectStore.createIndex("datum", "datum", { unique: false });
        objectStore.createIndex("status", "status", {unique: false});
        objectStore.createIndex("syncStatus", "syncStatus", {unique: false});
  }
}

//Get all tasks
function getAll(isdone) {
    const searchurl ="/get";
    //Open Transaction
    let transaction = db.transaction(["toDoApp"], "readwrite");
     //Log Success of transaction
     transaction.oncomplete = function() {
        console.log('Transaction sucessful');
    };
    //Log Error of transaction
    transaction.onerror = function() {
        console.log('Transaction error!');
    };
    //Clear actual list
    taskdiv = document.getElementById("taskdiv");
    taskdiv.innerHTML = "";
    //Defines active attribute in navbar
    updateNavbar(isdone);
    //Check if network connection is online or offline. If offline get data from local database.
    if(navigator.onLine){
        //Clear data of indexedDB
        clearData(transaction);
        //Fetch all tasks
        fetch(searchurl, {method: 'GET'}).then(response => {
            response.json().then(function (taskdata) {
                tasks= [];
                tasks = taskdata;
                //taskId = 1;
                //let taskToAddToDB = [];
                
                //Iterate over all tasks
                for(e in tasks) {
                    //Get the highest Task ID
                    // if(taskId < tasks[e].id){
                    //     taskId = tasks[e].id;
                    // }
                    //Create an entry for all tasks which meet the condition
                    if (tasks[e].status === isdone) {
                        var div = newTaskelement(tasks[e]);
                        taskdiv = document.getElementById("taskdiv");
                        taskdiv.appendChild(div);
                        //taskToAddToDB.push(tasks[e])
                    }
                };
                addTasksToDB(tasks);
            })
        });
    } else{
        let objectStore = db.transaction("toDoApp").objectStore("toDoApp");
        objectStore.openCursor().onsuccess = function(event) {
            let cursor = event.target.result;
            if(cursor){
                if(cursor.value.status === isdone){
                    var div = newTaskelementDB(cursor.value.id, cursor.value.taskTitle, cursor.value.status);
                    taskdiv = document.getElementById("taskdiv");
                    taskdiv.appendChild(div);
                }
                cursor.continue();
            }
        }
    }
}

//Create new Task and reload all
//Task is written in IndexDB for Offline functionality
function addTask(){
    let task = [{
        //id: taskId+1,
        taskTitle: document.getElementById("inputfield").value,
        datum: new Date().toISOString(),
        status: "false",
        syncStatus: "new-task"
    }];

    //Fetch add task
    fetch("/add", {
        method: 'POST', body: JSON.stringify(task[0]), headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status !== 200) {
                console.log(response);
            } else {
                //Get All Tasks
                getAll("false");
            }
            
        }).catch(err => {
            
            //Register Background Sync for new Tasks when fetch fails
            if("serviceWorker" in navigator && "SyncManager" in window){
                navigator.serviceWorker.ready.then(function(registration){
                    registration.sync.register("new-task")
                })
                //Open Transaction to add task to local DB
                let transaction = db.transaction(["toDoApp"], "readwrite");
                //Log Success of transaction
                transaction.oncomplete = function() {
                    console.log('Transaction sucessful');
                };
                //Log Error of transaction
                transaction.onerror = function() {
                    console.log('Transaction error!');
                };
            
                //Add Item
                let objectStore = transaction.objectStore("toDoApp");
                let objectStoreRequest = objectStore.add(task[0]);
                objectStoreRequest.onsuccess = function(event){
                    //When Sucessful emtpy array
                    console.log("Request was sucessful")
                    task.splice(0,1);
                }
                getAll('false');
            }
    })


    //Reset Inputfield
    document.getElementById("inputfield").value = "";
    document.getElementById("button-addon2").disabled = true;

}

//Set task status in dependency on the current status
function setDone(taskid, actualstate){
    let savestate;
    if(actualstate===false){
        actualstate="true"
        savestate = "false"
    }
    else {
        actualstate="false"
        savestate="true"
    }
    var gesuchterTask = {
        id: taskid,
        datum: new Date().toISOString(),
        status: actualstate
    };

    //Open Transaction to add task to local DB
    let transaction = db.transaction(["toDoApp"], "readwrite");
    //Log Success of transaction
    transaction.oncomplete = function() {
        console.log('Transaction sucessful');
    };
    //Log Error of transaction
    transaction.onerror = function() {
        console.log('Transaction error!');
    };

    //Update data of task wheither the task is done or not done
    let objectStore = transaction.objectStore("toDoApp");
    objectStore.openCursor().onsuccess = function(event){
        var cursor = event.target.result;
        if(cursor){
            if(cursor.value.id == taskid){
                if(cursor.value.status == "false"){
                    const updateData = cursor.value;
                    updateData.status = "true";
                    updateData.datum = gesuchterTask.datum;
                    if(cursor.value.syncStatus === "aktuell") 
                    {updateData.syncStatus = "updated-task"}
                    const request = cursor.update(updateData)
                    request.onsuccess = function(){
                        console.log("Update successful")
                    }
                } else {
                    const updateData = cursor.value;
                    updateData.status = "false";
                    updateData.datum = gesuchterTask.datum;
                    updateData.syncStatus = "updated-task";
                    const request = cursor.update(updateData)
                    request.onsuccess = function(){
                        console.log("Update successful")
                    }
                }
            }
            cursor.continue();
        }
    }

    fetch("/setDone", {
        method: 'PUT', body: JSON.stringify(gesuchterTask), headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status !== 200) {
                
            } else {
                response.json().then(function () {
                    //Reload tasks and rest in actual state
                    getAll(savestate);
                })
            }

    }).catch(err => {
        //Register Background Sync for updated Tasks when fetch fails
        if("serviceWorker" in navigator && "SyncManager" in window){
            navigator.serviceWorker.ready.then(function(registration){
                registration.sync.register("updated-task")
            })
        }
        getAll(savestate);
    })
}

//Alle löschen
function deleteAll(){
    fetch("/delete", {
        method: 'DELETE'
    }).then(response => {
        if(response.status == 200)
        console.log("alle gelöscht");
        getAll("false");
    }).catch(err => {
        console.log(err);
    })
}

//Check Input and en-/disable button
function checkinput(){
    var value = document.getElementById("inputfield").value;
    if (value ==="") {
        document.getElementById("button-addon2").disabled = true;
    }else{
        document.getElementById("button-addon2").disabled = false;
    }
}

//Generate new Taskelement for Frontend
function newTaskelement(e){
    var text = e.taskTitle;
    var id = e.id;
    var done = e.status;
    var div = document.createElement('DIV');
    div.classList.add('input-group', 'mb-3', 'row-container');
    html ='<div class="input-group-prepend checkbox-container">'+
    '<div class="input-group-text">'+
        '<input type="checkbox" onclick="setDone(' + id + ',' + done +')" selected aria-label="Checkbox for task">'+
    '</div>'+
    '</div>'+
    '<div class="alert alert-dark text-container" role="alert">'+
    '</div>';
    html = html.trim();
    div.innerHTML = html;
    div.lastChild.innerHTML = text;
    if(done === "false"){
        div.firstChild.firstChild.firstChild.checked = false;
   }
   else{
        div.firstChild.firstChild.firstChild.checked = true;
   }
    return div;
}

//Generate new Taskelement for Frontend coming from local Database
function newTaskelementDB(id, taskTitle, status) {
    var taskId = id; 
    var text = taskTitle;
    var done = status; 
    var div = document.createElement('DIV');
    div.classList.add('input-group', 'mb-3');
    html ='<div class="input-group-prepend checkbox-container">'+
   '<div class="input-group-text">'+
       '<input type="checkbox" onclick="setDone(' + taskId + ',' + done +')" selected aria-label="Checkbox for task">'+
   '</div>'+
   '</div>'+
   '<div class="alert alert-dark text-container" role="alert">'+
   '</div>';
   html = html.trim();
   div.innerHTML = html;
   div.lastChild.innerHTML = text;
   if(done === "false"){
        div.firstChild.firstChild.firstChild.checked = false;
   }
   else{
    div.firstChild.firstChild.firstChild.checked = true;
   }
   
   return div;
}

//Update navbar attributes
function updateNavbar(isdone){
    if(isdone === 'true'){
        var ToDo = document.getElementById("ToDo");
        var Done = document.getElementById("Done");
        ToDo.classList.remove('active');
        Done.classList.add('active');
    }else{
        var ToDo = document.getElementById("ToDo");
        var Done = document.getElementById("Done");
        ToDo.classList.add('active');
        Done.classList.remove('active');
    }
}

//Clear Data of Database 
function clearData(dbtransaction){
    //Open a read/write transaction
    let transaction = dbtransaction;

    //Report sucess of the transaction completing after all elements have been deleted
    transaction.oncomplete = function() {
        console.log("Transaction completed successfully");
    }
    //Report error of transaction
    transaction.onerror = function(){
        console.log("Transaction could not be completed");
    }

    //Create an object store on the transaction
    var objectStore = transaction.objectStore("toDoApp");

    //Make a request to clear all the date out of the object store
    var objectStoreRequest = objectStore.clear();

    objectStoreRequest.onsuccess = function() {
        console.log("Request successful");
    }

}

function addTasksToDB(array) {
    tasks = array;
    //Open Transaction to add task to local DB
    let transaction = db.transaction(["toDoApp"], "readwrite");
    //Log Success of transaction
    transaction.oncomplete = function() {
        console.log('Transaction sucessful');
    };
    //Log Error of transaction
    transaction.onerror = function() {
        console.log('Transaction error!');
    };

    for(e in tasks){
        //Add Item
        let objectStore = transaction.objectStore("toDoApp");
        let objectStoreRequest = objectStore.add(tasks[e]);
        objectStoreRequest.onsuccess = function(event){
            //When Sucessful emtpy array
            console.log("Request was sucessful")
        }
    }
}