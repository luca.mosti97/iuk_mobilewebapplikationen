console.log('Hello from SW')

var cacheName = 'MainCache'

self.addEventListener('beforeinstallprompt', function (e) {
  e.preventDefault();
  return false;
});

//Add Sync EventListener for new-task and update-task event
self.addEventListener("sync", function (event) {
  if (event.tag === "new-task") {
    event.waitUntil(syncTasks("new-task"));
  }
  if (event.tag === "updated-task") {
    event.waitUntil(syncTasks("updated-task"));
  }
});


// Cache our known resources during install
self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(cacheName)
    .then(cache => cache.addAll([
      '/',
      '../index.html',
      '../manifest.json',
      '../script.js',
      '../style.css',
      '../images/homescreen.png',
      '../images/homescreen-144.png',
      '../bootstrap/css/bootstrap.css'
    ]))
  );
});


// Cache any new resources as they are fetched
self.addEventListener('fetch', function (event) {
  console.log(event.request.url);
  event.respondWith(
    caches.match(event.request)
    .then(function (response) {
      if (response) {
        return response;
      }
      var fetchRequest = event.request.clone();

      return fetch(fetchRequest).then(
        function (response) {
          if (!response || response.status !== 200) {
            return response;
          }
          return response;
        }
      );
    })
  );
});

function syncTasks(syncStatus) {
  let db;
  var DBrequest = self.indexedDB.open("toDoApp", 4)
  DBrequest.onsuccess = function(event) {
    db = event.target.result;
    //Open Transaction to add task to local DB
    let transaction = db.transaction(["toDoApp"], "readwrite");
    //Log Success of transaction
    transaction.oncomplete = function () {
      console.log('Transaction sw sucessful');
    };
    //Log Error of transaction
    transaction.onerror = function () {
      console.log('Transaction error!');
    };
  
    //Update data of task wheither the task is done or not done
    let objectStore = db.transaction("toDoApp").objectStore("toDoApp");
    objectStore.openCursor().onsuccess = function (event) {
      var cursor = event.target.result;
      if (cursor) {
        if (cursor.value.syncStatus == syncStatus) {
          if (syncStatus === "updated-task") {
            const updateData = cursor.value;
            fetch("/setDone", {
              method: 'PUT',
              body: JSON.stringify(updateData),
              headers: {
                'Content-Type': 'application/json'
              }
            }).then(response => {
              if (response.status !== 200) {
                
                console.error(response);
                
              } 
            })
          } 
          if(syncStatus === "new-task") {
            const newData = cursor.value;
            fetch("/add", {
              method: 'POST',
              body: JSON.stringify(newData),
              headers: {
                'Content-Type': 'application/json'
              }
            }).then(response => {
              if (response.status !== 200) {
                
                console.error(response);
                
              } 
            })
          }
        }
        cursor.continue();
      }
    }
  }
}