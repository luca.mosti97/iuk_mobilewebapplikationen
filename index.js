//Applikationsvariablen
const express = require('express');
const app = express()
app.use(express.static('./public'));

//Bodyparser
const bodyParser = require('body-parser');
app.use(bodyParser.json());

//Variablen
var tasks = [];

//Localhost Port auf 3000
const PORT = process.env.PORT || 4000;
app.listen(PORT, () => console.log('Example app listening'));

//Hello World
app.get('/', (req, res) => res.send('Hello World!'))


//Task hinzufügen
app.post('/add', function(req, res){
    let daten = req.body;
    let taskTitle = daten.taskTitle;
    let datum = daten.datum;
    let id = daten.id;
    let status = daten.status;
    let syncStatus = "aktuell";
    let newTask = new taskelement(id, taskTitle, status, datum, syncStatus)
    tasks.push(newTask);
    res.send('Task added' + JSON.stringify(tasks));
})

//Tasks abfragen
app.get('/get', (req, res) => {
    let id = 1;
    tasks.forEach(element => {
        element.id = id;
        id++;
    });
    console.log(tasks)
    res.send(JSON.stringify(tasks));
})

//Tasks done setzen
app.put('/setDone', (req, res) => {
    let daten = req.body;
    let id = daten.id;
    let datum = daten.datum;
    let status = daten.status;
    console.log(daten.id)
    for(i in tasks) {
        if(tasks[i].id === id) {
            if(tasks[i].datum < datum){
                tasks[i].status = status;
                console.log(tasks[i])
            }
            res.send(JSON.stringify(tasks[i]))
        }
    }
})

//Alle Tasks löschen
app.delete('/delete', (req, res) => {
    tasks = [];
    res.send(true);
})

//neuer Task
function taskelement(id, taskTitle, status, datum, syncStatus){
    this.id = id;
    this.taskTitle = taskTitle;
    this.status = status;
    this.syncStatus = syncStatus;
    this.datum = datum
}